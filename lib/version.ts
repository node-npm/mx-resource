function toNumbers(s: string) {
    let ls = s.split(".")
    let nums: number[] = [];
    for (let i = 0; i < ls.length; i++) {
        nums.push(parseInt(ls[i]))
    }

    return nums;
}

// >=
function gecmp(a: string, b: string): boolean {
    let numAs = toNumbers(a);
    let numBs = toNumbers(b);

    for (let i = 0; i < Math.max(numAs.length, numBs.length); i++) {
        let na = numAs[i] || 0
        let nb = numBs[i] || 0

        if (na > nb) return true;
        if (na < nb) return false;
    }

    return true;
}

export var V_UnderV8 = !gecmp(process.versions.node, "9.0.0")