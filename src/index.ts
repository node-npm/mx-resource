import { createHash } from 'crypto';
import debug from 'debug';
import { EventEmitter } from 'events';
import fs from 'fs';
import { join } from 'path';
import { execPools } from '../lib/exec_pools';
import { BaseSource } from './source/base';
import { NetSource } from './source/net';
let Debug = debug('server-libs');

function error(...args: any[]) {
    Debug('%O', ...args);
}

export class ResourceModule<T> extends EventEmitter {
    static cache: { [key: string]: ResourceModule<any> } = {};

    /**
     * 监听资源
     * @param file 
     * @param syncNet 
     */
    static watch<T>(file: string, _package?: string, syncNet?: string, fullPath: boolean = false): ResourceModule<T> {
        if (this.cache.hasOwnProperty(file)) return this.cache[file];
        this.cache[file] = new ResourceModule<T>(file, _package, syncNet, fullPath);
        this.cache[file].init();
        return this.cache[file];
    }

    /**
     * 检查资源
     * @param thread 
     */
    static async checkResource(thread: number = 2) {
        let runningPool: any[] = [];
        for (let key in this.cache) {
            let k = this.cache[key];
            // await k.onlineCheck().catch(e => { error('checkResource', e) })
            runningPool.push(k.net.onlineCheck.bind(k));
        }

        await execPools(runningPool, thread)
        return true;
    }

    //-----------------------------------------------//

    public resData: { [tkey: string]: T } = {};
    public resDataOri: { [tkey: string]: T } = {};
    private file: string = '';
    private net: BaseSource;
    md5: string = '';
    private constructor(file: string, _package?: string, url?: string, fullPath: boolean = false) {
        super();
        if (fullPath || fs.existsSync(file)) {
            this.file = file;
        }
        else {
            this.file = join(process.cwd(), file);
        }

        this.net = new NetSource(_package || '', url || '', this.file, () => this.md5)
    }

    private init() {
        fs.watchFile(this.file, this._onFileChange.bind(this));
        this._loadFile();
        process.nextTick(() => {
            this.emit("change");
        })

        this.net.onlineCheck();
    }

    on(event: 'change', listener: (...args: any[]) => void): this {
        super.on(event, listener);
        return this;
    }

    once(event: 'change', listener: (...args: any[]) => void): this {
        super.once(event, listener);
        return this;
    }

    private _onFileChange(curr: fs.Stats, prev: fs.Stats) {
        if (prev.ctime.getTime() == 0) {
            //console.log('文件被创建!');
            this._loadFile();
        } else if (curr.ctime.getTime() == 0) {
            // console.log('文件被删除!')
            this.resData = {};
            this.md5 = '';
            this.emit('change');
        } else if (curr.mtime.getTime() != prev.mtime.getTime()) {
            // console.log('文件有修改');
            this._loadFile();

        }
    }

    private _loadFile() {
        let srcMd5 = this.md5;
        this.resData = {};
        this.resDataOri = {};
        this.md5 = '';

        try {
            let fileInfo = fs.readFileSync(this.file).toString();
            this.md5 = createHash('md5').update(fileInfo).digest('hex');
            let allData = JSON.parse(fileInfo);
            for (let key in allData) {
                if (!allData.hasOwnProperty(key) || key == 'Template') {
                    continue;
                }
                this.resData['k' + key] = allData[key];
                this.resDataOri[key] = allData[key];
            }
        }
        catch (e) {
            error('res error:' + this.file);
        }

        if (this.md5 != srcMd5) this.emit('change');
    }

    public getAllRes() {
        return this.resData;
    };

    public getOriAllRes() {
        return this.resDataOri;
    }

    public getRes(id: string): T | undefined {
        id = 'k' + id;
        if (this.resData.hasOwnProperty(id)) {
            return this.resData[id];
        }
        return undefined;
    };

    public has(id: string): boolean {
        id = 'k' + id;
        if (this.resData.hasOwnProperty(id)) {
            return true;
        }
        return false;
    }

    /**
     * 遍历
     * @param func 返回 true的时候跳出循环 
     */
    public forEach(func: (value: T, key: string, res: this) => boolean | void) {
        for (let key in this.resData) {
            if (func(this.resData[key], key.slice(1, key.length), this)) {
                break;
            }
        }
    }
}