export class BaseSource {
    package: string = "";
    url: string = "";
    file: string = ""
    md5: () => string;
    constructor(pack: string, url: string, file: string, md5: () => string) {
        this.package = pack;
        this.url = url;
        this.file = file;
        this.md5 = md5;
    }
    /**
     * 增加一个线上检查的过程
     */
    public async onlineCheck() {
        if (!this.url) return false;
        return true;
    }
}