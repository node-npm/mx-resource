import { existsSync, mkdirSync, writeFileSync } from "fs";
import { parse } from "path";
import { resolve } from "url";
import { http_quest } from "../../lib/HttpQuest";
import { BaseSource } from "./base";

export class NetSource extends BaseSource {
    constructor(pack: string, url: string, file: string, md5: () => string) {
        super(pack, url, file, md5)
    }
    /**
     * 增加一个线上检查的过程
     */
    public async onlineCheck() {
        if (!this.url) return false;
        let pFile = parse(this.file);
        let url = this.url;
        if (url[url.length - 1] != '/') {
            url = url + '/';
        }
        let result;
        try {
            result = await http_quest<{ code: number }>('get', resolve(url, './check'), {
                package: this.package,
                file: pFile.name + pFile.ext,
                md5: this.md5
            }, 0, null, { respon_type: 'json' })
        }
        catch (e) {
            // 网络异常的话就跳过算了
            return false;
        }

        if (result.code == 0) {
            // 表示资源是一致的
            return true;
        }
        let _result
        try {
            _result = await http_quest<{ code: number, text: string, md5: string }>('get', resolve(url, './download'), {
                package: this.package,
                file: pFile.name + pFile.ext
            }, 0, null, { respon_type: 'json' });
        }
        catch (e) {
            return false;
        }

        if (_result.code != 0) return false;
        // 这里测试一下是否ok
        try {
            // 测试一下数据
            JSON.parse(_result.text)
            if (!existsSync(pFile.dir)) mkdirSync(pFile.dir, { recursive: true });
            writeFileSync(this.file, _result.text, { flag: "w+" });
        }
        catch (e) {

        }
        return true;
    }
}